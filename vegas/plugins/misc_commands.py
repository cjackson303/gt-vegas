import json
import discord
import random
from discord.ext import commands
from discord.ext.commands import has_any_role

from .base import BasePlugin

# All plugins should inherit form BasePlugin
class kiwiPlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group

#Blame Jason
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="When things go wrong, there's only one person to blame!",
                      case_insensitive=True,
                      aliases=['blame-jason'])
    async def blame_jason(self ,ctx):
        await ctx.send("https://cdn.discordapp.com/attachments/514427899231600650/635942530181234723/Blame_Jason.png")
        await ctx.message.delete()

#Blanner's Backstory
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="Learn the heart wrenching true story of Blanners backstory. Find out the origin of his nickname, the ardious journey to becoming a mod, and finally how he ascending to becomind a weeb god.",
                      case_insensitive=True,
                      aliases=['blanners-backstory'])
    @has_any_role("Divine Theorist")
    async def blanners_backstory(self ,ctx):
        await ctx.send("https://cdn.discordapp.com/attachments/514427899231600650/635942530181234723/Blame_Jason.pnghttps://discordapp.com/channels/353694095220408322/353766146115371009/483965765201166338")
        await ctx.message.delete()

#Calendar
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="See the Community Events Calendar!",
                      case_insensitive=True)
    async def calendar(self ,ctx):
        await ctx.send("https://calendar.google.com/calendar/b/1?cid=YXBwcm92ZWRndGRAZ21haWwuY29t")
        await ctx.message.delete()

#Definitions
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="Learn the definitions for Theorizing")
    async def definitions(self ,ctx):
        await ctx.send("**Definitions**\n\nThese are the official definitions used on this server. Please ensure all of your work conforms to these definitions, anything else will not be accepted.\n\nhttps://www.youtube.com/watch?v=lqk3TKuGNBA")
        await ctx.message.delete()

#Delete Server
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="Delete the server!",
                      case_insensitive=True,
                      aliases=['delete-server'])
    async def delete_server(self ,ctx):
        await ctx.send("https://imgur.com/a/FLsNeWc")
        await ctx.message.delete()

#Honc
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="HonC moment",
                      case_insensitive=True)
    async def honc(self ,ctx):
        await ctx.send("https://i.imgur.com/vnBlEqXh.jpg")
        await ctx.message.delete()

#It's Hoopaning
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="It really is.",
                      case_insensitive=True,
                      aliases=['its-hoopaning'])
    async def its_hoopaning(self ,ctx):
        await ctx.send("https://media.discordapp.net/attachments/503665005015597066/587698424854347777/1b0.png")
        await ctx.message.delete()

#Just a Warn
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="It really is.",
                      case_insensitive=True,
                      aliases=['just-a-warn'])
    async def just_a_warn(self ,ctx):
        await ctx.send("https://media.discordapp.net/attachments/503665005015597066/595655436036734991/hippowarn.png")
        await ctx.message.delete()

#Kiwi is Here
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="When you're in dire need of assistance, and a kiwi just happens to be there.",
                      case_insensitive=True,
                      aliases=['kiwi-is-here'])
    async def kiwi_is_here(self ,ctx):
        image= ["https://cdn.discordapp.com/attachments/623868642223980557/794730057980444703/3u9KC2Nh.png",
        "https://cdn.discordapp.com/attachments/623868642223980557/794730218975789056/LKRpMsQh.png",]
        await ctx.send(f'{random.choice(image)}')
        await ctx.message.delete()

#Lurker
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="Call out lurkers in GT Style",
                      case_insensitive=True)
    async def lurker(self ,ctx):
        await ctx.send("https://i.imgur.com/t9Ocdomh.jpg")
        await ctx.message.delete()

#Meet The Mods
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="just copy paste this one",
                      case_insensitive=True,
                      aliases=['meet-the-mods'])
    async def meet_the_mods(self, ctx):
        embed = discord.Embed(title="Meet The Mods", colour=discord.Colour(0x66CCFF), description="**From top left to bottom right:**\n\nEMOD (mean and terrifying), Aster (Big Bird with a Ban Hammer), Chronic (Gay Angel Mom), Steven (A white Rabbit (for now)), Xilas (Newt in a Giraffe Onesie), and Blanners (an Anime Body Pillow (missing))")
        embed.set_image(url="https://i.imgur.com/C0CEqin.png")
        await ctx.send(embed=embed)
        await ctx.message.delete()

#No Herobrine
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="Herobrine is not canon.",
                      case_insensitive=True,
                      aliases=['no-herobrine'])
    async def no_herobrine(self ,ctx):
        await ctx.send("https://twitter.com/notch/status/1106688767014793216?lang=en\nhttps://twitter.com/notch/status/23849639551700992?lang=en\nhttps://twitter.com/notch/status/249940547895185408?lang=en\nhttps://twitter.com/notch/status/285808043160133632?lang=en")
        await ctx.message.delete()

#Not Very Cash Money
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="For when someone does something not cash money.",
                      case_insensitive=True,
                      aliases=['not-very-cash-money'])
    async def not_very_cash_money (self ,ctx):
        await ctx.send("https://imgur.com/a/o6Afqbi")
        await ctx.message.delete()

#Self Deprecation
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="It is forbidden!",
                      case_insensitive=True,
                      aliases=['self-deprecation'])
    async def self_deprecation(self ,ctx):
        await ctx.send("https://cdn.discordapp.com/attachments/503598228365770772/794381627555315732/2021_no_self_deprecation.png")
        await ctx.message.delete()

#Sleeping Chat
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="It's just sleeping!",
                      case_insensitive=True,
                      aliases=['sleeping-chat'])
    @has_any_role("Master Theorist", "Inhuman Theorist", "Limitless Theorist", "Legendary Theorist", "Pure  Theorist", "Sage Theorist", "Ascended Theorist", "Divine Theorist")
    async def sleeping_chat(self ,ctx):
        await ctx.send("https://cdn.discordapp.com/attachments/503598228365770772/691026353956454400/FdkpfKJ.png")
        await ctx.message.delete()

#This Troubles Me
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="For troubling issues.",
                      case_insensitive=True,
                      aliases=['this-troubles-me'])
    async def this_troubles_me(self ,ctx):
        await ctx.send("https://cdn.discordapp.com/attachments/514427899231600650/586730171525103620/sans_is_ness.jpg")
        await ctx.message.delete()

#Unofficial Rule 11
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    @commands.command(description="The Official Unofficial Rule 8",
                      case_insensitive=True,
                      aliases=['unofficial-rule-11'])
    async def unofficial_rule_11(self ,ctx):
        await ctx.send("Bruh, it's just a discord server.\n\nYou need to calm down my dude. You're doing all this because of something on a discord server? It's an internet chatroom for a youtube channel, everyone's just here to have a good time, calm the fuck down.")
        await ctx.message.delete()

def setup(bot):
        bot.add_cog(kiwiPlugin(bot))
