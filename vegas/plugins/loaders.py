from discord.ext import commands

from .base import BasePlugin
from discord.ext.commands import is_owner


class LoaderPlugin(BasePlugin):
    @commands.command(hidden=True)
    @is_owner()
    async def load(self, context, module : str):
        """Loads a module."""
        try:
            self.bot.load_extension("plugins." + module)
        except Exception as e:
            await context.send('\N{PISTOL}')
            await context.send('{}: {}'.format(type(e).__name__, e))
        else:
            await context.send('\N{OK HAND SIGN}')

    @commands.command(hidden=True)
    @is_owner()
    async def unload(self, context, module : str):
        """Unloads a module."""
        try:
            self.bot.unload_extension("plugins." + module)
        except Exception as e:
            await context.send('\N{PISTOL}')
            await context.send('{}: {}'.format(type(e).__name__, e))
        else:
            await context.send('\N{OK HAND SIGN}')

    @commands.command(name='reload', hidden=True)
    @is_owner()
    async def _reload(self, context, module : str):
        """Reloads a module."""
        try:
            self.bot.unload_extension("plugins." + module)
            self.bot.load_extension("plugins." + module)
        except Exception as e:
            await context.send('\N{PISTOL}')
            await context.send('{}: {}'.format(type(e).__name__, e))
        else:
            await context.send('\N{OK HAND SIGN}')

def setup(bot):
    bot.add_cog(LoaderPlugin(bot))
