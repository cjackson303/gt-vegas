import discord

from discord.ext import commands

from settings.base import RULES_TRANSFER

from settings.base import RULES_BODY

from settings.base import INTERESTS_DICT

from settings.base import INTERESTS_DICT1

from .base import BasePlugin


keywords = "\n".join(INTERESTS_DICT.keys())
searchterms = INTERESTS_DICT.keys()


class InfoPlugin(BasePlugin):
    @commands.group(aliases=["info", "i"])
    async def information(self, context):
        if context.invoked_subcommand is None:
            pass

    @information.command(description="Does MatPat have a discord?",
                         case_insensitive=True,
                         aliases=["mpd", "matpat", "matpat-discord"])
    async def matpat_discord(self, context):
        embed = discord.Embed(title="Does Matpat Have a Discord?",
                              description="As the [first message](https://discord.com/channels/353694095220408322/483967686540394506/735637644087656518) in <#483967686540394506> states, MatPat does not have Discord account. We are a community made and run server that MatPat has endorsed in his videos. For reaching out about theories and game suggestions, you should head to his [Twitter](https://twitter.com/MatPatGT) instead.",
                              color=context.author.top_role.color)
        await context.send(embed=embed)

    @information.command(description="Looks like someone needs a refresher.",
                         case_insensitive=True,
                         aliases=["si", "server-info"])
    async def server_info(self, context):
        embed = discord.Embed(title="#server-info Index",
                              description="[Events](https://discord.com/channels/353694095220408322/735635802960429087/735635967209373756)\n[Roles](https://discord.com/channels/353694095220408322/735635802960429087/735636045143474308)\n[Leveled Roles](https://discord.com/channels/353694095220408322/735635802960429087/735636099409641504)\n[Leveled Roles, cont.](https://discord.com/channels/353694095220408322/735635802960429087/735639337613656086)\n[Approved Roles](https://discord.com/channels/353694095220408322/735635802960429087/735639445772042260)\n[Approved Roles, cont.](https://discord.com/channels/353694095220408322/735635802960429087/735639476222558209)\n[Miscellaneous](https://discord.com/channels/353694095220408322/735635802960429087/735639540022247477)",
                              color=context.author.top_role.color)
        await context.send(embed=embed)

    @information.command(description="Looking for something in the server? Try here!",
                         case_insensitive=True,
                         aliases=["mii", "my-interest-is", "interest"])
    async def my_interest_is(self, context, *interest):
        interest = " ".join(interest)
        if interest:
            if '@everyone' in interest:
                await context.send('Mass ping detected--please try again without \'everyone\'.')
            elif '@here' in interest:
                await context.send('Mass ping detected--please try again without \'here\'.')
            else:
                for broad_Term in INTERESTS_DICT1:
                    if interest in INTERESTS_DICT1[broad_Term]:
                        interest = broad_Term
                if interest in searchterms:
                    embed = discord.Embed(title="My Interest is {}!".format(interest),
                                          description="Here are the results for your interest:",
                                          color=context.author.top_role.color)
                    embed.add_field(name=interest.title(), value=INTERESTS_DICT[interest], inline=False)
                else:
                    embed = discord.Embed(title="My Interest is Unknown!",
                                          description="Seems like you need some help with the search terms.",
                                          color=context.author.top_role.color)
                    embed.add_field(name="List of Keywords:", value=keywords, inline=False)
                await context.send(embed=embed)
        else:
            embed = discord.Embed(title="My Interest is Unknown!",
                                  description="Seems like you need some help with the search terms.",
                                  color=context.author.top_role.color)
            embed.add_field(name="List of Keywords:", value=keywords, inline=False)
            await context.send(embed=embed)

    @information.command(description="Looks like someone needs a refresher.",
                         case_insensitive=True,
                         aliases=["ttr", "thems-the-rules", "rule"])
    async def thems_the_rules(self, context, rule=None):
        if rule:
            if rule in RULES_TRANSFER:
                rule = RULES_TRANSFER[rule]
                embed = discord.Embed(title=rule,
                                      description=RULES_BODY[rule],
                                      color=context.author.top_role.color)
                embed.add_field(name="Punishments and Penalties",
                                value="[This explains the warning and penalty system.](https://discord.com/channels/353694095220408322/483967686540394506/735638956095438848)",
                                inline=False)
                await context.send(embed=embed)
            else:
                await context.send("You haven't given me an existing rule number. :pensive:")

        else:
            await context.send("You haven't given me a rule to check. :pensive:")


def setup(bot):
    bot.add_cog(InfoPlugin(bot))
