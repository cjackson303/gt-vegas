--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_xp_leveledrole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_xp_leveledrole (
    id integer NOT NULL,
    guild bigint NOT NULL,
    name character varying(255) NOT NULL,
    xp_required bigint NOT NULL
);


ALTER TABLE public.discord_xp_leveledrole OWNER TO postgres;

--
-- Name: discord_xp_leveledrole_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_xp_leveledrole_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_xp_leveledrole_id_seq OWNER TO postgres;

--
-- Name: discord_xp_leveledrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_xp_leveledrole_id_seq OWNED BY public.discord_xp_leveledrole.id;


--
-- Name: discord_xp_leveledrole id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_leveledrole ALTER COLUMN id SET DEFAULT nextval('public.discord_xp_leveledrole_id_seq'::regclass);


--
-- Name: discord_xp_leveledrole discord_xp_leveledrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_leveledrole
    ADD CONSTRAINT discord_xp_leveledrole_pkey PRIMARY KEY (id);


--
-- Name: discord_xp_leveledrole_guild_c0163692; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_xp_leveledrole_guild_c0163692 ON public.discord_xp_leveledrole USING btree (guild);


--
-- Name: discord_xp_leveledrole_xp_required_ce261177; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_xp_leveledrole_xp_required_ce261177 ON public.discord_xp_leveledrole USING btree (xp_required);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_xp_userxp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_xp_userxp (
    id integer NOT NULL,
    score bigint NOT NULL,
    multiplier double precision NOT NULL,
    multiplier_timeout integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.discord_xp_userxp OWNER TO postgres;

--
-- Name: discord_xp_userxp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_xp_userxp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_xp_userxp_id_seq OWNER TO postgres;

--
-- Name: discord_xp_userxp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_xp_userxp_id_seq OWNED BY public.discord_xp_userxp.id;


--
-- Name: discord_xp_userxp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_userxp ALTER COLUMN id SET DEFAULT nextval('public.discord_xp_userxp_id_seq'::regclass);


--
-- Name: discord_xp_userxp discord_xp_userxp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_userxp
    ADD CONSTRAINT discord_xp_userxp_pkey PRIMARY KEY (id);


--
-- Name: discord_xp_userxp discord_xp_userxp_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_userxp
    ADD CONSTRAINT discord_xp_userxp_user_id_key UNIQUE (user_id);


--
-- Name: discord_xp_userxp_score_0d97af93; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_xp_userxp_score_0d97af93 ON public.discord_xp_userxp USING btree (score);


--
-- Name: discord_xp_userxp discord_xp_userxp_user_id_65d5e3a7_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_xp_userxp
    ADD CONSTRAINT discord_xp_userxp_user_id_65d5e3a7_fk_discord_u FOREIGN KEY (user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

